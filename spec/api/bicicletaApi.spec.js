let Bicicleta = require("../../models/Bicicleta");

let request = require('request');

let server = require("../../bin/www");

describe ("Test de la API", () => {
    describe ("GET /", () =>{

        it("Devuelve status 200", () => {

            expect(Bicicleta.allBicis.length).toBe(0);

            let a = new Bicicleta(1, "Rojo", "Trek", [28.503789, -13.853296]);

            Bicicleta.add(a);

            request.get("http://localhost:3000/api/bicicletas", (error,response,body)=>{

                expect(response.statusCode).toBe(200);

            });

        });

    })

});

//POST

describe("POST /create", () => {
    it("Devuelve status 201", (done) => {
      let headers = {
        "Content-type": "application/JSON"
      };
      let aBici = '{"id": 10, "color": "Rojo", "modelo": "Trek", "lat": "28.503789", "long": "-13.853296"}';
      request.post({
        headers: headers,
        url: "http://localhost:3000/api/bicicletas/create",
        body: aBici
      }, (error, response, body) => {
        expect(response.statusCode).toBe(201);
        expect(Bicicleta.findById(10).color).toBe("Rojo");
        done();
      });
    });
  });

  //PUT
  
  describe("PUT /update", () => {
    it("Devuelve status 201", (done) => {
      let headers = {
        "Content-type": "application/JSON"
      };
      let aBici = '{"id": 10, "color": "Rojo", "modelo": "Trek", "lat": "28.503789", "long": "-13.853296"}';
      let nueva = new Bicicleta(4, "green", "prueba", [28.503789,-13.853296]);
      Bicicleta.add(nueva);
      request.put({
        headers: headers,
        url: "http://localhost:3000/api/bicicletas/4/update",
        body: aBici
      }, (error, response, body) => {
        expect(response.statusCode).toBe(201);
        expect(Bicicleta.findById(10).color).toBe("Rojo");
        done();
      });
    });
  });

  //DELETE
  
  describe("DELETE /delete", () => {
    it("Devuelve status 204", (done) => {
      let headers = {
        "Content-type": "application/JSON"
      };
      let nueva = new Bicicleta(4, "green", "prueba", [28.503789,-13.853296]);
      Bicicleta.add(nueva);
      request.delete({
        headers: headers,
        url: "http://localhost:3000/api/bicicletas/delete",
        body: '{"id" : 4}'
      }, (error, response, body) => {
        expect(response.statusCode).toBe(204);
        expect(Bicicleta.allBicis.length).toBe(0);
        done();
      });
    });
  });
