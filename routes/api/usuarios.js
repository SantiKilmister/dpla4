let express = require('express');

let router = express.Router();

let usuarioControllerAPI = require("../../controllers/api/usuarioControllerAPI");

//Get usuario
router.get("/", usuarioControllerAPI.usuario_list);

//Post create

router.post("/create", usuarioControllerAPI.usuario_create);

//Post usuario
router.post("/reservar", usuarioControllerAPI.usuario_reservar);


//Delete Usuario
router.delete("/delete", usuarioControllerAPI.usuario_delete);

//Actualizar usuario
router.put("/:_id/update", usuarioControllerAPI.usuario_update);





module.exports = router;