let express = require('express');

let router = express.Router();

let bicicletaControllerAPI = require("../../controllers/api/bicicletaControllerAPI");


//GET Bici
router.get("/", bicicletaControllerAPI.bicicleta_list);
//POST Bici
router.post("/create", bicicletaControllerAPI.bicicleta_create);
//Delete Bici
router.delete("/delete", bicicletaControllerAPI.bicicleta_delete);
//Actualizar Bici
router.put("/:id/update",bicicletaControllerAPI.bicicleta_update);




module.exports = router;
