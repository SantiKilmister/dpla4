var express = require('express');
var router = express.Router();

var bicicletaController = require("../controllers/bicicleta");

router.get("/", bicicletaController.bicicleta_list);

// ruta para metodo create

router.get("/create", bicicletaController.bicicleta_create_get);

//envio de formulario al back end

router.post("/create", bicicletaController.bicicleta_create_post);

//ruta delete

router.post("/:id/delete", bicicletaController.bicicleta_delete_post);

//ruta Actualizar GET

router.get("/:id/update", bicicletaController.bicicleta_update_get);

//ruta Post Actualizar

router.post("/:id/update", bicicletaController.bicicleta_update_post);



module.exports = router;
