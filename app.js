
// IMPORTS
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');






//Instantitions
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var usuariosRouter = require('./routes/api/usuarios');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var reservasAPIRouter = require("./routes/api/reservas");
var usuariosRouter = require("./routes/usuarios");
var tokenRouter = require("./routes/tokens");
var loginRouter = require("./routes/logins");
const passport = require("./config/passport");
const session = require("express-session");
var Usuario = require("./models/Usuario");
var Token = require("./models/Token");

//  Objeto session MEMORY STORE


const store = new session.MemoryStore;




var app = express();

//Configuarations
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
//Middleware
app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//ROUTERS
app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/bicicletas', bicicletasRouter);
app.use('usuarios',usuariosRouter);

app.use('/api/bicicletas', bicicletasAPIRouter); 
app.use('/api/usuarios',usuariosAPIRouter);
app.use("/api/reservas", reservasAPIRouter);
app.use("/token", tokenRouter);
app.use("/usuarios", usuariosRouter);
app.use("/logins", loginRouter);

app.get("/forgotPassword", function (req,res) {

  res.render("session/forgotPassword");
 
 });
 
 
 
 app.post("/forgotPassword", function (req,res) {
 
  Usuario.findOne({email: req.body.email}, function (err, usuario) {
 
  if (!usuario) return res.render("session/forgotPassword", {info: {message: "No existe ese email en nuestra BBDD."}});
 
 
 
  usuario.resetPassword(function(err){
 
  if (err) return next(err);
 
  console.log("session/forgotPasswordMessage");
 
  });
 
  res.render("session/forgotPasswordMessage"); 
 
  });
 
 });
 
 
 
 app.get("/resetPassword/:token", function (req, res, next) {
 
  Token.findOne({token: req.params.token}, function (err, token) {
 
  if (!token) return res.status(400).send({type: "not-verified", msg: "No existe un usuario asociado al token. Verifique que su token no haya expirado."});
 
 
 
  Usuario.findById(token._userId, function (err, usuario) {
 
  if (!usuario) return res.status(400).send({msg:"No existe un usuario asociado al token."});
 
  res.render("session/resetPassword", {errors: {}, usuario: usuario}); 
 
  }); 
 
  }); 
 
 });

 app.use('/bicicletas', loggedIn, bicicletasRouter);
 
 
 
 app.post("/resetPassword", function(req, res) {
 
  if (req.body.password != req.body.confirm_password) {
 
  res.render("session/resetPassword", {errors: {confirm_password: {message: "No coincide con el password introducido."}},
 
  usuario: new Usuario({email: req.body.email})});
 
  return;
 
  }
 
  Usuario.findOne({email: req.body.email}, function (err, usuario) {
 
  usuario.password = req.body.password,
 
  usuario.save(function (err) {
 
  if (err) {
 
  res.render("session/resetPassword", {errors: err.errors, usuario: new Usuario({email: req.body.email})});
 
  } else {
 
  res.redirect("/login");
 
  }
 
  }); 
 
  });
 
 });

app.use(passport.initialize());

app.use(passport.session());


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
//server bootup or server export

function loggedIn(req,res,next) {

  if (req.user) {
 
  next();
 
  } else {
 
  console.log("Usuario no logueado");
 
  res.redirect("/login");
 
  }
 
 }
 



app.post("/login", function (req,res,next) {

  passport.authenticate("local", function (err, usuario, info) {
 
  if (err) return next(err);
 
  if (!usuario) return res.render("session/login", {info});
 
  req.logIn(usuario, function (err) {
 
  if (err) return next(err);
 
  return res.redirect("/");
 
  });
 
  })(req,res,next);
 
 });
 
 
 
 app.get("/logout", function (req,res) {
 
  req.logOut(); //Limpiamos la sesión
 
  res.redirect("/");
 
 });


//Conexion mongoose
mongoose.connect('mongodb://localhost/red_bicicletas', { useUnifiedTopology: true, useNewUrlParser: true  }); 

mongoose.Promise = global.Promise;

var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)

db.on("error", console.error.bind('Error de conexión con MongoDB'));



module.exports = app;
