let Usuario = require('../../models/Usuario');

//Reserva usuario

exports.usuario_reservar = function(req,res){ 

    Usuario.findById(req.body._id, function(err,usuario) {
   
    if(err) res.status(500).send(err.message);
   
    console.log(usuario);
   
    usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err){
   
    console.log("Reservada!!");
   
    res.status(200).send();
   
    });
   
    }); 
   
   };

   //Listado
   exports.usuario_list = function(req, res) {

    Usuario.allUsuarios(function(err,usuarios){

        if(err){
    
          res.status(500).send(err.message);
        }
          res.status(200).send(usuarios);
      })

};

//Crear Usuario
exports.usuario_create = function(req,res){
    let usuario = ({

        nombre: req.body.nombre
      })
  
  
      Usuario.add(usuario,(err,NewUsuario)=>{
        if (err) 
        res.status(500).send(err.message);
  
        res.status(200).send(NewUsuario);
  
      });
  
    
};

exports.usuario_delete = function(req,res){

    //Borrado con la funcion remove
    Usuario.removeById(req.body._id,(err,NoUsuario)=>{
        if(err)
    
          res.status(500).send(err.message);
          res.status(200).send(NoUsuario);
    
          
    
    });

};

exports.usuario_update = function(req,res){


    
      Usuario.updateById(req.params._id,{nombre:req.body.nombre},function(err,nUsuario){
      if (err) 
      res.status(500).send(err.message);
  
      res.status(200).send(nUsuario);
  
    });
  }

