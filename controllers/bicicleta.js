let Bicicleta = require("../models/Bicicleta");

exports.bicicleta_list = function(req,res){
  Bicicleta.allBicis(function(err,bicis){

    if(err){

      res.status(500).send(err.message);
    }
      res.render("bicicletas/index", {bicis:bicis});
  })
     

 }

 exports.bicicleta_create_get = function(req,res) {

    res.render("bicicletas/create");

}

//Creacion bicicleta

exports.bicicleta_create_post = function(req,res) {

    let bici = ({

      bicicletaID: req.body.bicicletaID,
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [req.body.latitud, req.body.longitud]
    })


    Bicicleta.add(bici,(err,NewBici)=>{
      if (err) 
      res.status(500).send(err.message);

      res.redirect("/bicicletas"); //Redirecciono al index

    });

    
}


//BORRADO
exports.bicicleta_delete_post = function(req,res){

    //Borrado con la funcion remove
    Bicicleta.removeById(req.body.id,(err,NewBici)=>{
    if(err)

      res.status(500).send(err.message);
      res.redirect("/bicicletas");//Redireciono index

      

});

}

//UPDATE por get


exports.bicicleta_update_get = function(req,res) {
   Bicicleta.findById(req.params.id,function(err,nbici){
    //console.log(req.params.id);

    if(err) res.status(500).send(err.message);
    //console.log(nbici);
    res.render("bicicletas/update",{bici:nbici});
  }); 
}


//UPDATE por post


exports.bicicleta_update_post = function(req, res) {

  let bici = ({

    bicicletaID: req.body.id,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.latitud, req.body.longitud]

  })
    Bicicleta.updateById(bici,function(err,nbici){
    if (err) 
    res.status(500).send(err.message);

    res.redirect("/bicicletas"); //Redirecciono al index

  });
  

}