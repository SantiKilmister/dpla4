let mongoose = require("mongoose");

let Schema = mongoose.Schema;




let bicicletaSchema = new Schema({

 bicicletaID: Number,

 color: String,

 modelo: String,

 ubicacion: { type: [Number], index: true }

});

//Mostrar bicis

bicicletaSchema.statics.allBicis = function (cb) {
    return this.find({}, cb);

};

//Añadir bicis

bicicletaSchema.statics.add = function(aBici, cb) {

    return this.create(aBici, cb);

};






//let b = new Bicicleta(2, "Azul", "Orbea", [28.501367, -13.853476]);


 
//Bicicleta.add(b);


bicicletaSchema.statics.removeById = function(anId, cb) {

    return this.deleteOne({

        _id:anId
        },cb);

};

bicicletaSchema.statics.updateById = function(aBici, cb) {

    return this.updateOne(aBici,cb);

};



bicicletaSchema.statics.findById = function(anId,cb) {

    
    return this.findOne({ _id:anId },cb);
};
    


module.exports = mongoose.model ("Bicicleta", bicicletaSchema);
